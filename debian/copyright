Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pvr.zattoo
Source: https://github.com/rbuehlma/pvr.zattoo
Files-Excluded: depends

Files: *
Copyright: 2005-2022 Team Kodi
 2016-2022 rbuehlma
License: GPL-2+

Files: src/md5.cpp
 src/md5.h
Copyright: 1991-2, RSA Data Security, Inc
 2017, Frank Thilo (thilo@unix-ag.org)
License: Permissive~RSA
 License to copy and use this software is granted provided that
 it is identified as the "RSA Data Security, Inc.
 MD5 Message-Digest Algorithm" in all material mentioning or
 referencing this software or this function.
 .
 License is also granted to make and use derivative works provided
 that such works are identified as "derived from the RSA Data
 Security, Inc. MD5 Message-Digest Algorithm" in all material
 mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning either
 the merchantability of this software or the suitability of this
 software for any particular purpose. It is provided "as is"
 without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.
Comment:
 License retrieved from src/md5.h file of upstream tarball

Files: debian/*
Copyright: 2020-2022 Vasyl Gello <vasek.gello@gmail.com>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
